package com.wyb.spring.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author lenovo
 * @Date 2022/9/13 11:16
 * @Version 1.0
 * @Function
 */
@Configuration
public class MybatisPageConfig {
    @Bean
    public PaginationInterceptor paginationInterceptor(){
        return new PaginationInterceptor();
    }
}
