package com.wyb.spring.config;

import com.wyb.spring.interceptor.JwtInterceptor;
import com.wyb.spring.interceptor.ViewMappingInterceptor;
import org.apache.catalina.connector.Connector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.ResourceUtils;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @Description WebMvcConfig
 * @Author JiangHu
 * @Date 2022/9/8 10:34
 */
@Configuration
@AutoConfigureAfter(value = WebMvcAutoConfiguration.class)
public class WebMvcConfig implements WebMvcConfigurer {

	@Autowired
	private ResourceConfigBean resourceConfigBean;
	@Autowired
	private ViewMappingInterceptor viewMappingInterceptor;
	@Autowired
	private JwtInterceptor jwtInterceptor;

	@Bean
	public Connector connector() {
		Connector connector = new Connector();
		connector.setScheme("http");
		connector.setPort(80);
		return connector;
	}

	@Bean
	public ServletWebServerFactory servletWebServerFactory() {
		TomcatServletWebServerFactory factory = new TomcatServletWebServerFactory();
		factory.addAdditionalTomcatConnectors(connector());
		return factory;
	}

	//静态资源的配置
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		String osName = System.getProperty("os.name");
		if (osName.toLowerCase().startsWith("win")) {
			// 将本地文件夹 注册为 系统的一个路径
			registry.addResourceHandler(resourceConfigBean.getResourcePartern())
					.addResourceLocations(ResourceUtils.FILE_URL_PREFIX +
							resourceConfigBean.getWindowsLocal());
		} else  {
			registry.addResourceHandler(resourceConfigBean.getResourcePartern())
					.addResourceLocations(ResourceUtils.FILE_URL_PREFIX +
							resourceConfigBean.getLinuxLocal());
		}
	}

	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**")
				.allowedOrigins("*")
				.allowedMethods("POST", "GET", "PUT", "OPTIONS", "DELETE")
				.maxAge(3600)
				.allowedOriginPatterns("*");
	}

	//拦截器的配置
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(viewMappingInterceptor).addPathPatterns("/**");
//		registry.addInterceptor(jwtInterceptor).addPathPatterns("/**");
	}
}
