package com.wyb.spring.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @Description ResourceConfigBean
 * @Author JiangHu
 * @Date 2022/9/8 14:50
 */
@Component
public class ResourceConfigBean {
	@Value("${spring.resource.path}")
	private String resourcePath;
	@Value("${spring.resource.path.partern}")
	private String resourcePartern;
	@Value("${spring.resource.local.windows}")
	private String windowsLocal;
	@Value("${spring.resource.local.linux}")
	private String LinuxLocal;

	public String getResourcePath() {
		return resourcePath;
	}

	public void setResourcePath(String resourcePath) {
		this.resourcePath = resourcePath;
	}

	public String getResourcePartern() {
		return resourcePartern;
	}

	public void setResourcePartern(String resourcePartern) {
		this.resourcePartern = resourcePartern;
	}

	public String getWindowsLocal() {
		return windowsLocal;
	}

	public void setWindowsLocal(String windowsLocal) {
		this.windowsLocal = windowsLocal;
	}

	public String getLinuxLocal() {
		return LinuxLocal;
	}

	public void setLinuxLocal(String linuxLocal) {
		LinuxLocal = linuxLocal;
	}
}
