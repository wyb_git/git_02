package com.wyb.spring.config.shiro;

import at.pollux.thymeleaf.shiro.dialect.ShiroDialect;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @Author lenovo
 * @Date 2022/9/23 22:52
 * @Version 1.0
 * @Function
 */
@Configuration
public class shiroConfig {
    @Autowired
    private MyShiro myShiro;
    @Bean
    public DefaultSecurityManager defaultSecurityManager(){
        DefaultWebSecurityManager manager = new DefaultWebSecurityManager();
        manager.setRealm(myShiro);
        return manager;
    }

    @Bean
    public ShiroFilterFactoryBean shiroFilterFactoryBean(){
        ShiroFilterFactoryBean factoryBean = new ShiroFilterFactoryBean();
        //设置安全管理器
        factoryBean.setSecurityManager(defaultSecurityManager());
        // 设置登录地址和登录成功地址
        factoryBean.setLoginUrl("/login");
        factoryBean.setSuccessUrl("/test/thymeleafTest");

        /**
         * 设置访问策略 ---- 设置过滤器
         * 策略放在 map 中，请求过来，找对对应的 url 匹配策略名称，
         * 通过该名称找到对应的过滤器，使用过滤器来处理请求
         * anon：匿名访问，无需登录 ---- AnonymousFilter
         * authc：登录后才能访问 ---- FormAuthenticationFilter
         * user：登录过能访问 ---- UserFilter
         * logout：登出 ---- LogoutFilter
         *
         */
        Map<String, String> map = new LinkedHashMap<>();
        // 注册登录
        map.put("/login", "anon");
        map.put("/login2", "anon");
        map.put("/logout", "anon");
        map.put("/register", "anon");
        // 静态资源
        map.put("/css/**", "anon");
        map.put("/image/**", "anon");
        map.put("/images/**", "anon");
        map.put("/js/**", "anon");
        map.put("/vendors/**", "anon");
        map.put("/static/**", "anon");
        // common
        map.put("/common/403", "anon");
        map.put("/common/404", "anon");
        map.put("/common/500", "anon");
        // api
        map.put("/api/**", "anon");

        // 登录验证策略
        map.put("/**", "authc");
        factoryBean.setFilterChainDefinitionMap(map);

        return factoryBean;
    }

    /**
     * - 注册shiro方言，让 thymeleaf 支持 shiro 标签
     */
    @Bean
    public ShiroDialect shiroDialect(){
        return new ShiroDialect();
    }

    /**
     * DefaultAdvisorAutoProxyCreator, Advisor 代理类生成器
     */
    @Bean
    @DependsOn({"lifecycleBeanPostProcessor"})
    public DefaultAdvisorAutoProxyCreator advisorAutoProxyCreator(){
        DefaultAdvisorAutoProxyCreator advisorAutoProxyCreator = new DefaultAdvisorAutoProxyCreator();
        advisorAutoProxyCreator.setProxyTargetClass(true);
        return advisorAutoProxyCreator;
    }

    /**
     * - 创建 AuthorizationAttributeSourceAdvisor，扫描 Shiro 注解
     */
    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(){
        AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
        authorizationAttributeSourceAdvisor.setSecurityManager(defaultSecurityManager());
        return authorizationAttributeSourceAdvisor;
    }
}
