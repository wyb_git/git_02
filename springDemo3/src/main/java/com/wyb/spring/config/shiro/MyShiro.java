package com.wyb.spring.config.shiro;

import com.wyb.spring.modules.account.entity.Resource;
import com.wyb.spring.modules.account.entity.Role;
import com.wyb.spring.modules.account.entity.User;
import com.wyb.spring.modules.account.service.ResourceService;
import com.wyb.spring.modules.account.service.RoleService;
import com.wyb.spring.modules.account.service.UserService;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author lenovo
 * @Date 2022/9/23 20:00
 * @Version 1.0
 * @Function
 */
@Component
public class MyShiro extends AuthorizingRealm {
    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private ResourceService resourceService;
    @Override//授权
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principal) {
        SimpleAuthorizationInfo sai = new SimpleAuthorizationInfo();
        User user = (User) principal.getPrimaryPrincipal();
        List<Role> roles = roleService.getRolesByUserId(user.getId());
        //System.out.println(roles);
        roles.stream().forEach(item->{
            //System.out.println(item.getRoleName());
            sai.addRole(item.getRoleName());
            List<Resource> resources = resourceService.getResourcesByRoleId(item.getId());
            resources.stream().forEach(it->{
                sai.addStringPermission(it.getPermission());
            });
        });
        return sai;
    }

    @Override//认证
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        String  userName = (String) token.getPrincipal();
        User user = userService.getUserByUserName(userName);
        if (user == null) {
            throw new UnknownAccountException("用户名或密码错误");
        }

        return new SimpleAuthenticationInfo(user,user.getPassword(),getName());
    }
}
