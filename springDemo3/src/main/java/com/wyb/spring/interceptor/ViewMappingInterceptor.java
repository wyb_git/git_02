package com.wyb.spring.interceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Author lenovo
 * @Date 2022/9/16 11:41
 * @Version 1.0
 * @Function
 */
@Component
public class ViewMappingInterceptor implements HandlerInterceptor {
    private static final Logger LOGGER = LoggerFactory.getLogger(ViewMappingInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
                             Object handler) throws Exception {
        LOGGER.debug("------pre Mapping-------");
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response,
                           Object handler, ModelAndView modelAndView) throws Exception {
        LOGGER.debug("------post Mapping-------");
        if (modelAndView == null || modelAndView.getViewName().startsWith("redirect") ||
                modelAndView.getViewName().startsWith("forward")) {
            return;
        }
        String path = request.getServletPath();
        String template = (String) modelAndView.getModelMap().get("template");
        if (template == null) {
            modelAndView.getModelMap().addAttribute("template",
                    path.substring(1,path.length()));
        }
        HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response,
                                Object handler, Exception ex) throws Exception {
        LOGGER.debug("------after Mapping-------");
        //HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
    }
}
