package com.wyb.spring.interceptor;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.sfac.springBoot.utils.JwtUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Author lenovo
 * @Date 2022/10/10 21:41
 * @Version 1.0
 * @Function
 */
@Component
public class JwtInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //如果请求不是直接映射到方法，则跳过
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }

        String path = request.getServletPath();
        if (path.contains("login")) {
            return true;
        }
        //获取请求头中的jwt token字符串
        String jwtToken = request.getHeader("Token");
        if (StringUtils.isBlank(jwtToken)) {
            throw new Exception("no jwt token");
        }
        //获取签发对象
        String audience = JwtUtil.getAudience(jwtToken);
        //验签
        DecodedJWT result = JwtUtil.verifyToken(jwtToken, audience);
        if (result == null) {
            throw new Exception("jwt token verify failed");
        }
        return HandlerInterceptor.super.preHandle(request, response, handler);
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
    }
}
