package com.wyb.spring.modules.test.service.impl;

import com.wyb.spring.modules.test.dao.CountryDao;
import com.wyb.spring.modules.test.entity.Country;
import com.wyb.spring.modules.test.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author lenovo
 * @Date 2022/9/10 8:56
 * @Version 1.0
 * @Function
 */
@Service
public class CountryServiceImpl implements CountryService {
    @Autowired
    private CountryDao countryDao;

    /**
     * @param countryId
     * @return
     */
    @Override
    public Country getCountryByCountryId(int countryId) {
        return countryDao.getCountryByCountryId(countryId);
    }
}
