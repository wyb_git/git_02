package com.wyb.spring.modules.account.entity;

import javax.persistence.*;

/**
 * @Description UserRole
 * @Author JiangHu
 * @Date 2022/9/20 14:11
 */
@Entity
@Table(name = "account_user_role")
public class UserRole {
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(updatable = false, nullable = true)
	private Integer id;
	private int userId;
	private int roleId;

	public UserRole() {
	}

	public UserRole(int userId, int roleId) {
		this.userId = userId;
		this.roleId = roleId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
}
