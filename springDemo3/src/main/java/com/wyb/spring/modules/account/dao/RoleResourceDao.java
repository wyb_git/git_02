package com.wyb.spring.modules.account.dao;

import com.wyb.spring.modules.account.entity.RoleResource;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.springframework.stereotype.Repository;

/**
 * @Description: Role Resource Dao
 * @author: HymanHu
 * @date: 2021年2月21日
 */
@Mapper
@Repository
public interface RoleResourceDao {
	
	@Insert("insert into account_role_resource (role_id, resource_id) values (#{roleId}, #{resourceId})")
	@Options(useGeneratedKeys = true, keyColumn = "id", keyProperty = "id")
	void insertRoleResource(RoleResource roleResource);
	
	@Delete("delete from account_role_resource where resource_id = #{resourceId}")
	void deleteRoleResourceByResourceId(int resourceId);
	
	@Delete("delete from account_role_resource where role_id = #{roleId}")
	void deleteRoleResourceByRoleId(int roleId);
}
