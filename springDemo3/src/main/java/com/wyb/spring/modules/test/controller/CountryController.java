package com.wyb.spring.modules.test.controller;

import com.wyb.spring.modules.test.entity.Country;
import com.wyb.spring.modules.test.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author lenovo
 * @Date 2022/9/10 8:58
 * @Version 1.0
 * @Function
 */
@RestController
@RequestMapping("/api")
public class CountryController {
    @Autowired
    private CountryService countryService;

    /**
     * 127.0.0.1/api/country/522 ---get
     */
    @GetMapping("/country/{countryId}")
    public Country getCountryByCountryId(@PathVariable int countryId) {
        return countryService.getCountryByCountryId(countryId);
    }
}
