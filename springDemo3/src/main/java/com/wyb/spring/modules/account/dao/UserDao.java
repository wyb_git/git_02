package com.wyb.spring.modules.account.dao;


import com.wyb.spring.modules.account.entity.User;
import com.wyb.spring.modules.common.vo.Search;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Description: User Dao
 * @author: HymanHu
 * @date: 2021年2月21日
 */
@Mapper
@Repository
public interface UserDao {

	@Select("select * from account_user where user_name = #{userName} and password = #{password}")
	User getUserByUserNameAndPassword(@Param("userName") String userName, @Param("password") String password);
	
	@Select("select * from account_user where user_name = #{userName}")
	List<User> getUserByUserName(String userName);
	
	@Insert("insert into account_user (user_name, password, user_image, " +
			"create_date_time, update_date_time) "
			+ "values (#{userName}, #{password}, #{userImage}, " +
			"#{createDateTime}, #{updateDateTime})")
	@Options(useGeneratedKeys = true, keyColumn = "id", keyProperty = "id")
	void insertUser(User user);
	
	@Update("update account_user set user_name = #{userName}, "
			+ "user_image = #{userImage} where id = #{id}")
	void updateUser(User user);
	
	@Update("update account_user set password = #{password} where id = ${id}")
	void updateUserPassword(@Param("password") String password, @Param("id") int id);
	
	@Select("select * from account_user where id = #{id}")
	@Results(id="userResult", value={
		@Result(column="id", property="id"),
		@Result(column="id", property="roles",
			javaType=List.class,
			many=@Many(select="com.wyb.spring.modules.account.dao.RoleDao.getRolesByUserId"))
	})
	User getUserById(int id);
	
	@Delete("delete from account_user where id = #{id}")
	void deleteUserById(int id);
	
	@Select("<script>"
			+ "select * from account_user "
			+ "<where> "
			+ "<if test='keyword != \"\" and keyword != null'>"
			+ " and (user_name like '%${keyword}%') "
			+ "</if>"
			+ "</where>"
			+ "<choose>"
			+ "<when test='sort != \"\" and sort != null'>"
			+ " order by ${sort} ${direction}"
			+ "</when>"
			+ "<otherwise>"
			+ " order by id desc"
			+ "</otherwise>"
			+ "</choose>"
			+ "</script>")
	List<User> getUsersBySearchVo(Search search);
}
