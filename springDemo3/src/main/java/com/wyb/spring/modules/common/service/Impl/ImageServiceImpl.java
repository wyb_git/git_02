package com.wyb.spring.modules.common.service.Impl;

import com.wyb.spring.config.ResourceConfigBean;
import com.wyb.spring.modules.common.dao.ExceptionLogDao;
import com.wyb.spring.modules.common.entity.ExceptionLog;
import com.wyb.spring.modules.common.service.ImageService;
import com.wyb.spring.modules.common.vo.Result;
import com.wyb.spring.utils.FileUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Locale;

/**
 * @Author lenovo
 * @Date 2022/9/16 18:00
 * @Version 1.0
 * @Function
 */
@Service
public class ImageServiceImpl implements ImageService {

    @Autowired
    private ResourceConfigBean resourceConfigBean;
    @Value("${project.artifactId}")
    private String artifactId;

    @Override
    public Result<String> uploadImage(MultipartFile file, String imageTypeName) {
        // 校验图片，类型、尺寸……
        if (file.isEmpty()) {
            return new Result<>(Result.ResultStatus.FAILED.code, "Image is empty.");
        }

        if (!FileUtil.isImage(file)) {
            return new Result<>(Result.ResultStatus.FAILED.code, "This file is not image.");
        }

        // TODO 判断图片尺寸

        String imageName = String.format("%s.%s",
                System.currentTimeMillis(),
                FileUtil.getFileType(file.getOriginalFilename()));
        String osName = System.getProperty("os.name");
        // 判断目标文件夹是否存在，不存在则创建
        String destFolderString = String.format("%s%s/%s",
                osName.toLowerCase().startsWith("win") ?
                        resourceConfigBean.getWindowsLocal() :
                        resourceConfigBean.getLinuxLocal(),
                artifactId,
                imageTypeName
        );
        File destFolder = new File(destFolderString);
        if (!destFolder.exists()) {
            destFolder.mkdirs();
        }

        // 上传文件到指定的位置，并返回 url 访问地址
        String destFilePath = String.format("%s/%s",
                destFolderString,
                imageName
        );
        String ImageUrl = String.format("%s%s/%s/%s",
                resourceConfigBean.getResourcePath(),
                artifactId,
                imageTypeName,
                imageName
        );

        try {
            file.transferTo(new File(destFilePath));
        } catch (Exception e) {
            e.printStackTrace();
            return new Result<>(
                    Result.ResultStatus.FAILED.code, "Upload image failed");
        }

        return new Result<>(
                Result.ResultStatus.SUCCESS.code,
                "Upload image success",
                ImageUrl);
    }
}
