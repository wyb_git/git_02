package com.wyb.spring.modules.common.service.Impl;

import com.wyb.spring.modules.common.dao.ExceptionLogDao;
import com.wyb.spring.modules.common.entity.ExceptionLog;
import com.wyb.spring.modules.common.service.ExceptionLogService;
import com.wyb.spring.modules.common.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * @Author lenovo
 * @Date 2022/9/20 16:09
 * @Version 1.0
 * @Function
 */
@Service
public class ExceptionLogServiceImpl implements ExceptionLogService {
    @Autowired
    private ExceptionLogDao exceptionLogDao;
    @Override
    public Result<ExceptionLog> insertExceptionLog(ExceptionLog ex) {
        ExceptionLog temp = exceptionLogDao.getExceptionLogByParam(ex.getPath(),
                ex.getMethodName(), ex.getExceptionType());
        if (temp != null) {
            return new Result<>(Result.ResultStatus.SUCCESS.code, "异常已记录");
        }
        ex.setCreateDateTime(LocalDateTime.now());
        ex.setUpdateDateTime(LocalDateTime.now());
        exceptionLogDao.insertExceptionLog(ex);
        return new Result<>(Result.ResultStatus.SUCCESS.code, "添加异常成功", ex);
    }
}
