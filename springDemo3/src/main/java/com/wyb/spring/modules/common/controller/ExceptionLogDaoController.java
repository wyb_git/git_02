package com.wyb.spring.modules.common.controller;

import com.wyb.spring.modules.common.entity.ExceptionLog;
import com.wyb.spring.modules.common.service.ExceptionLogService;
import com.wyb.spring.modules.common.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author lenovo
 * @Date 2022/9/20 16:06
 * @Version 1.0
 * @Function
 */
@RestController
@RequestMapping("/api")
public class ExceptionLogDaoController {

    @Autowired
    private ExceptionLogService exceptionLogService;

    /**
     * 127.0.0.1/api/exceptionLog ---- post
     * {"ip":"127.0.0.1","path":"/api/city","className":"CityController",
     * "methodName":"getCityById","exceptionType":"NullPointException",
     * "exceptionMessage":"*******"}
     */
    @PostMapping("/exceptionLog")
    public Result<ExceptionLog> insertExceptionLog(@RequestBody ExceptionLog ex) {
        return exceptionLogService.insertExceptionLog(ex);
    }
}
