package com.wyb.spring.modules.account.controller;

import com.github.pagehelper.PageInfo;

import com.wyb.spring.modules.account.entity.Resource;
import com.wyb.spring.modules.account.service.ResourceService;
import com.wyb.spring.modules.common.vo.Result;
import com.wyb.spring.modules.common.vo.Search;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Description: Resource Controller
 * @author: HymanHu
 * @date: 2021年2月21日
 */
@RestController
@RequestMapping("/api")
public class ResourceController {

	@Autowired
	private ResourceService resourceService;
	
	/**
	 * 127.0.0.1/api/resource ---- post
	 * {"permission":"deleteUser"}
	 */
	@PostMapping(value = "/resource", consumes = "application/json")
	public Result<Resource> insertResource(@RequestBody Resource resource) {
		return resourceService.insertResource(resource);
	}
	
	/**
	 * 127.0.0.1/api/resource ---- put
	 * {"id":"1","permission":"deleteUser"}
	 */
	@PutMapping(value = "/resource", consumes = "application/json")
	public Result<Resource> updateResource(@RequestBody Resource resource) {
		return resourceService.updateResource(resource);
	}
	
	/**
	 * 127.0.0.1/api/resource/2 ---- delete
	 */
	@DeleteMapping("/resource/{id}")
	public Result<Object> deleteResourceById(@PathVariable int id) {
		return resourceService.deleteResourceById(id);
	}
	
	/**
	 * 127.0.0.1/api/resource/1 ---- get
	 */
	@GetMapping("/resource/{id}")
	public Resource getResourceById(@PathVariable int id) {
		return resourceService.getResourceById(id);
	}
	
	/**
	 * 127.0.0.1/api/resourceList ---- post
	 * {"currentPage":"1","pageSize":"5","orderBy":"id","direction":"desc","keyWord":""}
	 */
	@PostMapping(value = "/resourceList", consumes = "application/json")
	public PageInfo<Resource> getResourcesBySearchBean(@RequestBody Search search) {
		return resourceService.getResourcesBySearchBean(search);
	}
}
