package com.wyb.spring.modules.test.service.impl;

import com.wyb.spring.modules.common.vo.Result;
import com.wyb.spring.modules.common.vo.Search;
import com.wyb.spring.modules.test.dao.StudentDao;
import com.wyb.spring.modules.test.entity.Student;
import com.wyb.spring.modules.test.service.StudentService;
import jdk.nashorn.api.scripting.ScriptUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Author lenovo
 * @Date 2022/9/13 17:52
 * @Version 1.0
 * @Function
 */
@Service
public class StudentServiceImpl implements StudentService {
    @Autowired
    private StudentDao studentDao;

    @Override
    @Transactional
    public Result<Student> insertStudent(Student student) {
        studentDao.saveAndFlush(student);
        return new Result<>(Result.ResultStatus.SUCCESS.code, "insert success",student);
    }

    @Override
    @Transactional
    public Result<Student> updateStudent(Student student) {
        //与插入是同样的方法，区别是传有id
        studentDao.saveAndFlush(student);
        return new Result<>(Result.ResultStatus.SUCCESS.code, "update success",student);
    }

    @Override
    @Transactional
    public Result<Student> deleteStudent(Integer id) {
        studentDao.deleteById(id);
        return new Result<>(Result.ResultStatus.SUCCESS.code, "delete success");
    }

    @Override
    public Student getStudentById(Integer id) {
//        return studentDao.getById(id);
        return studentDao.getStudentById(id);
    }

    @Override
    public Page<Student> getStudentBySearch(Search search) {

        search.init();
        Sort.Direction direction = StringUtils.isBlank(search.getDirection())||
                search.getDirection().toLowerCase().equals("asc")?Sort.Direction.ASC:Sort.Direction.DESC;
        String order = StringUtils.isBlank(search.getSort())?"id":search.getSort();
        Sort sort = Sort.by(direction,order);
        Pageable pageable = PageRequest.of(search.getCurrentPage()-1,search.getPageSize(),sort);


        //构造Example对象，创建查询模板，匹配规则对象
        Student student = new Student();
        student.setName(search.getKeyword());
        ExampleMatcher matcher = ExampleMatcher.matchingAny()
                .withMatcher("name",match->match.contains())
                .withIgnorePaths("age");
        Example<Student> example = Example.of(student, matcher);

        studentDao.findAll( example,  pageable);
        return null;
    }

    @Override
    public Student findFirstByName(String name) {
        return studentDao.findFirstByName(name);
    }
}
