package com.wyb.spring.modules.account.controller;

import com.github.pagehelper.PageInfo;

import com.wyb.spring.modules.account.entity.User;
import com.wyb.spring.modules.account.service.UserService;
import com.wyb.spring.modules.common.vo.Result;
import com.wyb.spring.modules.common.vo.Search;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Description: User Controller
 * @author: HymanHu
 * @date: 2021年2月21日
 */
@RestController
@RequestMapping("/api")
public class UserController {
	
	@Autowired
	private UserService userService;

	/**
	 * 127.0.0.1/api/login ---- post
	 * {"userName":"admin", "password":"111111"}
	 */
	@PostMapping(value = "/login", consumes = "application/json")
	public Result<User> login(@RequestBody User user) {
		return userService.login(user);
	}

	@PostMapping(value = "/login2", consumes = "application/json")
	public Result<String> login2(@RequestBody User user) {
		return userService.login2(user);
	}
	
	/**
	 * 127.0.0.1/api/user ---- post
	 * {"userName":"admin", "password":"111111"}
	 */
	@PostMapping(value = "/user", consumes = "application/json")
	public Result<User> insertUser(@RequestBody User user) {

		return userService.insertUser(user);
	}
	
	/**
	 * 127.0.0.1/api/user ---- put
	 * {"id":"1", "userName":"admin", "password":"111111"}
	 */
	@PutMapping(value = "/user", consumes = "application/json")
	public Result<User> updateUser(@RequestBody User user) {
		return userService.updateUser(user);
	}
	
	/**
	 * 127.0.0.1/api/user/1 ---- get
	 */
	@GetMapping("/user/{id}")
	public User getUserById(@PathVariable int id) {
		return userService.getUserById(id);
	}
	
	/**
	 * 127.0.0.1/api/user/1 ---- delete
	 */
	@DeleteMapping("/user/{id}")
//	@RequiresPermissions(value = "deleteUser")
	public Result<Object> deleteUserById(@PathVariable int id) {
		return userService.deleteUserById(id);
	}
	
	/**
	 * 127.0.0.1/api/users ---- post
	 * {"currentPage":"1","pageSize":"5","orderBy":"id","direction":"desc","keyWord":""}
	 */
	@PostMapping(value = "/users", consumes = "application/json")
	public PageInfo<User> getUsersBySearchBean(@RequestBody Search search) {
		return userService.getUsersBySearchBean(search);
	}
}
