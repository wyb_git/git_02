package com.wyb.spring.modules.account.controller;

import com.wyb.spring.modules.account.service.UserService;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Description AccountController
 * @Author JiangHu
 * @Date 2022/9/21 14:07
 */
@Controller
public class AccountController {

	@Autowired
	private UserService userService;
	/**
	 * 127.0.0.1/login ---- get
	 */
	@GetMapping("/login")
	public String loginPage(ModelMap modelMap) {
		modelMap.addAttribute("template", "account/login");
		return "index3";
	}

	/**
	 * 127.0.0.1/register ---- get
	 */
	@GetMapping("/register")
	public String registerPage(ModelMap modelMap) {
		modelMap.addAttribute("template", "account/register");
		return "index3";
	}

	/**
	 * 127.0.0.1/account/users ---- get
	 */
	@GetMapping("/account/users")
	@RequiresRoles(value={"admin", "manager"}, logical= Logical.OR)
	public String userPage() {
		return "index";
	}

	@GetMapping("/account/roles")
	@RequiresRoles(value={"admin", "manager"}, logical= Logical.OR)
	public String rolePage() {
		return "index";
	}

	@GetMapping("/logout")
	public String logout(){
		userService.logout();
		return "redirect:login";
	}
}
