package com.wyb.spring.modules.test.dao;

import com.wyb.spring.modules.test.entity.Country;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author lenovo
 * @Date 2022/9/10 8:53
 * @Version 1.0
 * @Function
 */
@Mapper
@Repository
public interface CountryDao {

    @Select("select * from test_country where country_id = #{countryId}")
    @Results(id = "countryResult",value = {
            //没有该语句country_id会为空，因为和cities映射了，所以需要重新指明
            @Result(property = "countryId",column = "country_id"),
                    //返回的数据
            @Result(property = "cities",
                    //传入指定方法的查询字段
                    column = "country_id",
                    javaType = List.class,
                    many = @Many(select = "com.wyb.spring.modules.test.dao.CityDao.getCitiesByCountryId")
            )
    })
    Country getCountryByCountryId(int countryId);
}
