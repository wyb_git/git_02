package com.wyb.spring.modules.account.controller;

import com.github.pagehelper.PageInfo;

import com.wyb.spring.modules.account.entity.Role;
import com.wyb.spring.modules.account.service.RoleService;
import com.wyb.spring.modules.common.vo.Result;
import com.wyb.spring.modules.common.vo.Search;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Description: Role Controller
 * @author: HymanHu
 * @date: 2021年2月21日
 */
@RestController
@RequestMapping("/api")
public class RoleController {

	@Autowired
	private RoleService roleService;
	
	/**
	 * 127.0.0.1/api/role ---- post
	 * {"roleName":"admin"}
	 */
	@PostMapping(value = "/role", consumes = "application/json")
	public Result<Role> insertRole(@RequestBody Role role) {
		return roleService.insertRole(role);
	}
	
	/**
	 * 127.0.0.1/api/role ---- put
	 * {"id":"1", "roleName":"admin"}
	 */
	@PutMapping(value = "/role", consumes = "application/json")
	public Result<Role> updateRole(@RequestBody Role role) {
		return roleService.updateRole(role);
	}
	
	/**
	 * 127.0.0.1/api/role/3 ---- delete
	 */
	@DeleteMapping("/role/{id}")
	public Result<Object> deleteRoelById(@PathVariable int id) {
		return roleService.deleteRoelById(id);
	}
	
	/**
	 * 127.0.0.1/api/role/1 ---- get
	 */
	@GetMapping("/role/{id}")
	public Role getRoleById(@PathVariable int id) {
		return roleService.getRoleById(id);
	}
	
	/**
	 * 127.0.0.1/api/roles ---- get
	 */
	@GetMapping("/roles")
	public List<Role> getRoles() {
		return roleService.getRoles();
	}
	
	/**
	 * 127.0.0.1/api/roles ---- post
	 * {"currentPage":"1","pageSize":"5","orderBy":"id","direction":"desc","keyWord":""}
	 */
	@PostMapping(value = "/roles", consumes = "application/json")
	public PageInfo<Role> getRolesBySearchBean(@RequestBody Search search) {
		return roleService.getRolesBySearchBean(search);
	}

	/**
	 * 127.0.0.1/api/users/19 ---- get
	 */
	@GetMapping(value = "/users/{userId}")
	public List<Role> getRolesByUserId(@PathVariable int userId) {
		return roleService.getRolesByUserId(userId);
	}
}
