package com.wyb.spring.modules.test.controller;

import com.wyb.spring.modules.common.vo.Result;
import com.wyb.spring.modules.common.vo.Search;
import com.wyb.spring.modules.test.entity.Student;
import com.wyb.spring.modules.test.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 * @Author lenovo
 * @Date 2022/9/13 17:56
 * @Version 1.0
 * @Function
 */
@RestController
@RequestMapping("/api")
public class StudentController {
    @Autowired
    private StudentService studentService;

    /**
     * 127.0.0.1/api/student ---post
     * {"name":"wyb","age":15}
     * {"name":"wyb","age":15,"studentCard":{"cardNo":"0001"}}
     */
    @PostMapping(value = "/student",consumes = MediaType.APPLICATION_JSON_VALUE)
    public Result<Student> insertStudent(@RequestBody Student student) {
        return studentService.insertStudent(student);
    }

    /**
     * 127.0.0.1/api/student ---put
     * {"id":1,"name":"wyb","age":15}
     */
    @PutMapping(value = "/student",consumes = MediaType.APPLICATION_JSON_VALUE)
    public Result<Student> updateStudent(@RequestBody Student student) {
        return studentService.updateStudent(student);
    }

    /**
     * 127.0.0.1/api/student/1 ---delete
     */
    @DeleteMapping(value = "/student/{id}")
    public Result<Student> deleteStudent(@PathVariable Integer id) {
        return studentService.deleteStudent(id);
    }

    /**
     * 127.0.0.1/api/student/1 ---get
     */
    @GetMapping(value = "/student/{id}")
    public Student getStudentById(@PathVariable Integer id) {
        return studentService.getStudentById(id);
    }

    /**
     * 127.0.0.1/api/students ---post
     * -在 jpa 中，sort 字段传递的不再是列名，而是属性名
     * {"currentPage":1,"pageSize":5,"sort":"id","direction":"desc","keyword":""}
     */
    @PostMapping(value = "/students",consumes = MediaType.APPLICATION_JSON_VALUE)
    public Page<Student> getStudentBySearch(@RequestBody Search search) {
        return studentService.getStudentBySearch(search);
    }


    /**
     * 127.0.0.1/api/student?name=wyb1 ---get
     *
     */
    @GetMapping(value = "/student1/{name}")
    public Student findFirstByName(@PathVariable String name) {
        return studentService.findFirstByName(name);
    }

}
