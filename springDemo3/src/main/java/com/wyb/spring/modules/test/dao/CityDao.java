package com.wyb.spring.modules.test.dao;


import com.wyb.spring.modules.common.vo.Search;
import com.wyb.spring.modules.test.entity.City;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author lenovo
 * @Date 2022/9/9 13:50
 * @Version 1.0
 * @Function
 */
@Mapper
@Repository
public interface CityDao {

	@Insert("insert into test_city (city_name,local_city_name,country_id,district," +
			"population,date_modified,date_created) values(#{cityName},#{localCityName},#{countryId}," +
			"#{district},#{population},#{dateModified},#{dateCreated})")
	//自增主键生成并返回
	@Options(useGeneratedKeys = true,keyColumn = "city_id",keyProperty = "cityId")
	void insertCity(City city);

	@Select("select * from test_city where city_name = #{cityName} limit 1")
	City getCityByCityName(String cityName);

	@Update("update test_city set city_name = #{cityName}, local_city_name = #{localCityName}," +
			"date_modified = #{dateModified} where city_id = #{cityId}")
	void updateCity(City city);


	@Delete("delete from test_city where city_id = #{cityId}")
	void deleteCityByCityId(int cityId);

	@Select("select * from test_city where city_id = #{cityId}")
	City getCityByCityId(int cityId);

	@Select("select * from test_city where country_id = #{countryId}")
	List<City> getCitiesByCountryId(int countryId);

	@Select("<script>"
			+ "select * from test_city "
			+ "<where> "
			+ "<if test='keyword != \"\" and keyword != null'>"
			+ " and (city_name like '%${keyword}%' or "
			+ " local_city_name like '%${keyword}%') "
			+ "</if>"
			+ "</where>"
			+ "<choose>"
			+ "<when test='sort != \"\" and sort != null'>"
			+ " order by ${sort} ${direction}"
			+ "</when>"
			+ "<otherwise>"
			+ " order by city_id desc"
			+ "</otherwise>"
			+ "</choose>"
			+ "</script>")
	List<City> getCityBySearch(Search search);
}
