package com.wyb.spring.modules.test.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wyb.spring.modules.test.entity.City;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @Author lenovo
 * @Date 2022/9/13 11:21
 * @Version 1.0
 * @Function
 */
@Mapper
@Repository
public interface CityDao2 extends BaseMapper<City> {
}
