package com.wyb.spring.modules.common.service;

import com.wyb.spring.modules.common.entity.ExceptionLog;
import com.wyb.spring.modules.common.vo.Result;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Author lenovo
 * @Date 2022/9/16 17:59
 * @Version 1.0
 * @Function
 */
public interface ImageService {
    Result<String> uploadImage(MultipartFile file, String imageTypeName);

}
