package com.wyb.spring.modules.common.controller;

import com.wyb.spring.modules.common.service.ImageService;
import com.wyb.spring.modules.common.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Author lenovo
 * @Date 2022/9/16 18:33
 * @Version 1.0
 * @Function
 */
@RestController
@RequestMapping("/api")
public class ImageController {
    @Autowired
    private ImageService imageService;

    /**
     * 127.0.0.1/api/image/profile-small ---post
     */
    @PostMapping(value = "/image/{imageTypeName}",consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Result<String> uploadImage(@RequestParam MultipartFile file,
                                      @PathVariable String imageTypeName) {
        return imageService.uploadImage(file, imageTypeName);
    }
}
