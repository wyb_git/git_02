package com.wyb.spring.modules.account.service;

import com.github.pagehelper.PageInfo;

import com.wyb.spring.modules.account.entity.User;
import com.wyb.spring.modules.common.vo.Result;
import com.wyb.spring.modules.common.vo.Search;

import java.util.List;

/**
 * @Description: User Service
 * @author: HymanHu
 * @date: 2021年2月21日
 */
public interface UserService {
	
	User getUserByUserNameAndPassword(String userName, String password);
	
	Result<User> login(User user);


	Result<String> login2(User user);

	void logout();

	Result<User> insertUser(User user);

	Result<User> updateUser(User user);
	
	User getUserById(int id);

	Result<Object> deleteUserById(int id);
	
	PageInfo<User> getUsersBySearchBean(Search search);
	
	User getUserByUserName(String userName);
	
	List<User> getUsersByKeyWord(String keyword);
}
