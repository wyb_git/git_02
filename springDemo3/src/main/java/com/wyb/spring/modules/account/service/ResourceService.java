package com.wyb.spring.modules.account.service;

import com.github.pagehelper.PageInfo;

import com.wyb.spring.modules.account.entity.Resource;
import com.wyb.spring.modules.common.vo.Result;
import com.wyb.spring.modules.common.vo.Search;

import java.util.List;

/**
 * @Description: Resource Service
 * @author: HymanHu
 * @date: 2021年2月21日
 */
public interface ResourceService {

	Result<Resource> insertResource(Resource resource);

	Result<Resource> updateResource(Resource resource);

	Result<Object> deleteResourceById(int id);
	
	Resource getResourceById(int id);
	
	List<Resource> getResourcesByRoleId(int roleId);
	
	PageInfo<Resource> getResourcesBySearchBean(Search search);
}
