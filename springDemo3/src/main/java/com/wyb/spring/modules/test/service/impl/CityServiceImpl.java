package com.wyb.spring.modules.test.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wyb.spring.aop.MyAnnotation;
import com.wyb.spring.modules.common.vo.Result;
import com.wyb.spring.modules.common.vo.Search;
import com.wyb.spring.modules.test.dao.CityDao;
import com.wyb.spring.modules.test.dao.CityDao2;
import com.wyb.spring.modules.test.entity.City;

import com.wyb.spring.modules.test.service.CityService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @Author lenovo
 * @Date 2022/9/9 16:51
 * @Version 1.0
 * @Function
 */
@Service
public class CityServiceImpl implements CityService {


	@Autowired
	private CityDao cityDao;
	@Autowired
	private CityDao2 cityDao2;
	@Override
	@Transactional
	public Result<City> insertCity(City city) {
		City temp = cityDao.getCityByCityName(city.getCityName());
		if (temp != null) {
			return new Result<City>(Result.ResultStatus.FAILED.code, "cityName is repeat");
		}
		city.setDateCreated(new Date());
		city.setDateModified(new Date());
		//cityDao.insertCity(city);
		cityDao2.insert(city);
		return new Result<>(Result.ResultStatus.SUCCESS.code, "insert success",city);
	}

	@Override
	@Transactional
	public Result<City> updateCity(City city) {
		City temp = cityDao.getCityByCityName(city.getCityName());
		if (temp != null && temp.getCityId() != city.getCityId()) {
			return new Result<>(Result.ResultStatus.FAILED.code, "cityName is not exit");
		}
		city.setDateModified(new Date());
		//cityDao.updateCity(city);
		UpdateWrapper<City> wrapper = new UpdateWrapper<>();
		wrapper.set(StringUtils.isBlank(city.getCityName()), "city_name", city.getCityName());
		wrapper.set(StringUtils.isBlank(city.getLocalCityName()), "local_city_name", city.getLocalCityName());
		wrapper.eq(true, "city_id", city.getCityId());
		cityDao2.update(city,wrapper);
		return new Result<>(Result.ResultStatus.SUCCESS.code, "update success",city);
	}

	@Override
	@Transactional
	public Result<City> deleteCityByCityId(int cityId) {
		//cityDao.deleteCityByCityId(cityId);
		cityDao2.deleteById(cityId);
		return new Result<>(Result.ResultStatus.SUCCESS.code, "delete success");
	}

	@Override
	public City getCityByCityId(int cityId) {

		//return cityDao.getCityByCityId(cityId);
		return cityDao2.selectById(cityId);
	}

	@Override
	@MyAnnotation
	public List<City> getCitiesByCountryId(int countryId) {
		return Optional.ofNullable(cityDao.getCitiesByCountryId(countryId)).orElse(Collections.emptyList());
	}

	@Override
	public PageInfo<City> getCityBySearch(Search search) {
		search.init();
		PageHelper.startPage(search.getCurrentPage(), search.getPageSize());
		return PageInfo.of(Optional.ofNullable(cityDao.getCityBySearch(search)).orElse(Collections.emptyList()));
	}

	@Override
	public Page<City> getCityBySearch2(Search search) {
		Page page = new Page<>(search.getCurrentPage(), search.getPageSize());
		QueryWrapper<City> wrapper = new QueryWrapper<>();
		wrapper.like(StringUtils.isBlank(search.getKeyword()), "city_name", search.getKeyword())
				.or().like(StringUtils.isBlank(search.getKeyword()), "local_city_name", search.getKeyword())
				.orderBy(true,
						StringUtils.isBlank(search.getDirection())
								|| search.getDirection().toLowerCase().equals("asc"),
						StringUtils.isBlank(search.getSort()) ? "city_id" : search.getSort()
				);
		cityDao2.selectMapsPage(page, wrapper);
		return null;
	}
}
