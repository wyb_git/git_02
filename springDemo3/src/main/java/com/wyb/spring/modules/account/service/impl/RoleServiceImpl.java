package com.wyb.spring.modules.account.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wyb.spring.modules.account.dao.RoleDao;
import com.wyb.spring.modules.account.dao.RoleResourceDao;
import com.wyb.spring.modules.account.dao.UserRoleDao;
import com.wyb.spring.modules.account.entity.Role;
import com.wyb.spring.modules.account.service.RoleService;
import com.wyb.spring.modules.common.vo.Result;
import com.wyb.spring.modules.common.vo.Search;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * @Description: Role Service Impl
 * @author: HymanHu
 * @date: 2021年2月21日
 */
@Service
public class RoleServiceImpl implements RoleService {
	
	@Autowired
	private RoleDao roleDao;
	@Autowired
	private UserRoleDao userRoleDao;
	@Autowired
	private RoleResourceDao roleResourceDao;

	@Override
	@Transactional
	public Result<Role> insertRole(Role role) {
		role.setCreateDateTime(LocalDateTime.now());
		roleDao.insertRole(role);
		return new Result<Role>(Result.ResultStatus.SUCCESS.code,
				"Insert success", role);
	}

	@Override
	@Transactional
	public Result<Role> updateRole(Role role) {
		roleDao.updateRole(role);
		return new Result<Role>(Result.ResultStatus.SUCCESS.code,
				"Update success", role);
	}

	@Override
	@Transactional
	public Result<Object> deleteRoelById(int id) {
		roleDao.deleteRoelById(id);
		userRoleDao.deleteUserRoleByRoleId(id);
		roleResourceDao.deleteRoleResourceByRoleId(id);
		return new Result<Object>(Result.ResultStatus.SUCCESS.code, "Delete success");
	}

	@Override
	public Role getRoleById(int id) {
		return roleDao.getRoleById(id);
	}

	@Override
	public List<Role> getRolesByUserId(int userId) {
		return Optional.ofNullable(roleDao.getRolesByUserId(userId)).orElse(Collections.emptyList());
	}

	@Override
	public List<Role> getRoles() {
		return Optional.ofNullable(roleDao.getRoles()).orElse(Collections.emptyList());
	}

	@Override
	public PageInfo<Role> getRolesBySearchBean(Search search) {
		search.init();
		PageHelper.startPage(search.getCurrentPage(), search.getPageSize());
		return new PageInfo<Role>(Optional
				.ofNullable(roleDao.getRolesBySearchBean(search))
				.orElse(Collections.emptyList()));
	}

}
