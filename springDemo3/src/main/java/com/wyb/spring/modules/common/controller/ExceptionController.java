package com.wyb.spring.modules.common.controller;

import com.wyb.spring.modules.common.entity.ExceptionLog;
import com.wyb.spring.modules.common.service.ExceptionLogService;
import org.apache.shiro.authz.UnauthorizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import javax.servlet.http.HttpServletRequest;
import java.lang.annotation.Annotation;

/**
 * @Author lenovo
 * @Date 2022/9/20 16:31
 * @Version 1.0
 * @Function
 */
@ControllerAdvice
public class ExceptionController  {
    @Autowired
    private ExceptionLogService exceptionLogService;

    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionController.class);

    @ExceptionHandler(value = {Exception.class,NoHandlerFoundException.class})
    public ModelAndView exceptionHandle(HttpServletRequest request, Exception exception) {
        exception.printStackTrace();
        LOGGER.debug(exception.getMessage());
        int code = 200;
        String message = "";
        String data = "";
        //将算术异常作为没有权限
        if (exception instanceof UnauthorizedException) {
            code = 403;
            message = "没有权限";
            data = "/common/403";
        } else if (exception instanceof NoHandlerFoundException) {
            code = 404;
            message = "没有该控制器";
            data = "/common/404";
        } else {
            code = 500;
            message = "服务器内部错误";
            data = "/common/500";
        }
        insertExceptionLog(request, exception);
        // 判断是否为接口
        // 包装数据返回不同的 modelAndView
        if (judgeInterface(request)) {
            ModelAndView mad = new ModelAndView(new MappingJackson2JsonView());
            mad.addObject("status", code);
            mad.addObject("message", message);
            mad.addObject("data", data);
            return mad;
        }
        return new ModelAndView("redirect:" + data);
    }

    private boolean judgeInterface(HttpServletRequest request) {
        HandlerMethod handlerMethod = (HandlerMethod) request.getAttribute
                ("org.springframework.web.servlet.HandlerMapping.bestMatchingHandler");
        RestController[] rs1 = handlerMethod.getBeanType().getDeclaredAnnotationsByType(RestController.class);
        RestController rss = handlerMethod.getBeanType().getDeclaredAnnotation(RestController.class);
        ResponseBody[] rs2 = handlerMethod.getBeanType().getDeclaredAnnotationsByType(ResponseBody.class);
        ResponseBody[] rs3 = handlerMethod.getMethod().getDeclaredAnnotationsByType(ResponseBody.class);
        return rs1.length > 0 || rs2.length > 0 || rs3.length > 0;
    }

    private void insertExceptionLog(HttpServletRequest request, Exception exception) {
        ExceptionLog el = new ExceptionLog();
        el.setIp(request.getRemoteAddr());
        el.setPath(request.getServletPath());
        HandlerMethod handlerMethod = (HandlerMethod) request.getAttribute
                ("org.springframework.web.servlet.HandlerMapping.bestMatchingHandler");
        el.setClassName(handlerMethod.getBeanType().getName());
        el.setMethodName(handlerMethod.getMethod().getName());
        el.setExceptionType(exception.getClass().getName());
        el.setExceptionMessage(exception.getMessage());
        exceptionLogService.insertExceptionLog(el);
    }
}
