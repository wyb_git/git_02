package com.wyb.spring.modules.test.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.pagehelper.PageInfo;
import com.wyb.spring.modules.common.vo.Result;
import com.wyb.spring.modules.common.vo.Search;
import com.wyb.spring.modules.test.entity.City;
import com.wyb.spring.modules.test.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Description CityController
 * @Author JiangHu
 * @Date 2022/9/9 11:06
 */
@RestController
@RequestMapping("/api")
public class CityController {
	@Autowired
	private CityService cityService;

	/**
	 * 127.0.0.1/api/city ---- post
	 * {"cityName":"hj","localCityName":"胡江","countryId":522}
	 */
	@PostMapping(value = "/city", consumes = MediaType.APPLICATION_JSON_VALUE)
	public Result<City> insertCity(@RequestBody City city) {

		return cityService.insertCity(city);
	}


	/**
	 * 127.0.0.1/api/city ---- put
	 */
	//@ModelAttribute 该注释解析表单
	@PutMapping(value = "/city", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public Result<City> updateCity(@ModelAttribute City city) {

		return cityService.updateCity(city);
	}

	/**
	 * 127.0.0.1/api/city/2261 ---- delete
	 */
	@DeleteMapping(value = "/city/{cityId}")
	public Result<City> deleteCity(@PathVariable int cityId) {

		return cityService.deleteCityByCityId(cityId);
	}

	/**
	 * 127.0.0.1/api/city/2261 ---- get
	 */
	@GetMapping(value = "/city/{cityId}")
	public City getCityByCityId(@PathVariable int cityId) {

		return cityService.getCityByCityId(cityId);
	}

	/**
	 * 查询列表集合
	 * 127.0.0.1/api/cities/522----get
	 */
	@GetMapping(value = "/cities/{countryId}")
	public List<City> getCitiesByCountryId(@PathVariable int countryId) {
		return cityService.getCitiesByCountryId(countryId);
	}

	/**
	 * 127.0.0.1/api/cities -----post
	 * 查询中的特例，涉及敏感信息，需要post提交，和登录一样
	 * {"currentPage":1,"pageSize":5,"sort":"city_id","direction":"desc","keyword":""}
	 */
	@PostMapping(value = "/cities",consumes = MediaType.APPLICATION_JSON_VALUE)
	public PageInfo<City> getCityBySearch(@RequestBody Search search) {
		return cityService.getCityBySearch(search);
	}


	/**
	 * 127.0.0.1/api/cities -----post
	 * 查询中的特例，涉及敏感信息，需要post提交，和登录一样
	 * {"currentPage":1,"pageSize":5,"sort":"city_id","direction":"desc","keyword":""}
	 */
	@PostMapping(value = "/cities1",consumes = MediaType.APPLICATION_JSON_VALUE)
	public Page<City> getCityBySearch2(@RequestBody Search search) {
		return cityService.getCityBySearch2(search);
	}

}
