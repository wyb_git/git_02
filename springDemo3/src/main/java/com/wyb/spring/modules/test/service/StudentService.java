package com.wyb.spring.modules.test.service;

import com.wyb.spring.modules.common.vo.Result;
import com.wyb.spring.modules.common.vo.Search;
import com.wyb.spring.modules.test.entity.Student;
import org.springframework.data.domain.Page;

/**
 * @Author lenovo
 * @Date 2022/9/13 17:51
 * @Version 1.0
 * @Function
 */
public interface StudentService {
    Result<Student> insertStudent(Student student);

    Result<Student> updateStudent(Student student);

    Result<Student> deleteStudent(Integer id);

    Student getStudentById(Integer id);

    Page<Student> getStudentBySearch(Search search);

    Student findFirstByName(String name);
}
