package com.wyb.spring.modules.common.vo;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * @Author lenovo
 * @Date 2022/9/16 17:52
 * @Version 1.0
 * @Function
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ImageEnum {
    PROFILE_BIG("profile-big", 1000, 800, 1024),
    PROFILE_MIDDLE("profile-middle", 500, 400, 512),
    PROFILE_SMALL("profile-small", 100, 80, 256)
    ;
    public String name;
    public int width;
    public int height;
    public int size;

    ImageEnum(String name, int width, int height, int size) {
        this.name = name;
        this.width = width;
        this.height = height;
        this.size = size;
    }

    public static ImageEnum getImageEnumByName(String name) {
        for (ImageEnum item : ImageEnum.values()) {
            if (item.name.equals(name)) {
                return item;
            }
        }
        return ImageEnum.PROFILE_SMALL;
    }

}
