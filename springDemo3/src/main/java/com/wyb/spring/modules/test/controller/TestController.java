package com.wyb.spring.modules.test.controller;

import com.wyb.spring.config.ResourceConfigBean;
import com.wyb.spring.modules.test.entity.City;
import com.wyb.spring.modules.test.service.CityService;
import org.apache.el.util.ReflectionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceEditor;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.swing.plaf.TableHeaderUI;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author lenovo
 * @Date 2022/9/14 18:09
 * @Version 1.0
 * @Function
 */
@Controller
@RequestMapping("/test")
public class TestController {

    @Autowired
    private CityService cityService;

    @Autowired
    private ResourceConfigBean resourceConfigBean;


    /**
     * 127.0.0.1/test/vueTest ---- get
     */
    @GetMapping("/vueTest")
    public String vueTestPage() {
        return "index";
    }

    /**
     * 127.0.0.1/test/file?fileName=QQ截图20220916144757.jpg ---- get
     */
    @GetMapping(value = "/file")
    private ResponseEntity<Resource> downloadFile(@RequestParam String fileName) {
        try {
            Resource resource = new UrlResource(
                    String.format("%s%s%s",
                    ResourceUtils.FILE_URL_PREFIX,
                    resourceConfigBean.getWindowsLocal(),
                    fileName));
            fileName = new String(fileName.getBytes("utf-8"), "ISO-8859-1");
            return ResponseEntity
                    .ok()
                    .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_OCTET_STREAM_VALUE)
                    .header(HttpHeaders.CONTENT_DISPOSITION,
                            String.format("attachment;filename=\"%s\"", fileName))
                    .body(resource);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @PostMapping(value = "/file",consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public String uploadFile(@RequestParam MultipartFile file,
                             RedirectAttributes redirectAttributes) {
        if (file.isEmpty()) {
            redirectAttributes.addFlashAttribute("message",
                    "please select file");
            return "redirect:/test/thymeleafTest";
        }
        try {
            String destFilePath = String.format("%s%s", resourceConfigBean.getWindowsLocal(),
                    file.getOriginalFilename());
            String fileUrl = String.format("%s%s", resourceConfigBean.getResourcePartern(),
                    file.getOriginalFilename());
            file.transferTo(new File(destFilePath));
            redirectAttributes.addFlashAttribute("message","upload success");
            redirectAttributes.addFlashAttribute("message", fileUrl);
            return "redirect:/test/thymeleafTest";
        } catch (IOException e) {
            e.printStackTrace();
            redirectAttributes.addFlashAttribute("message", "upload failed");
            return "redirect:/test/thymeleafTest";
        }

    }
    /**
     * 127.0.0.1/test/thymeleafTest
     */
    @GetMapping(value = "/thymeleafTest")
    public String thymeleafPage(ModelMap modelMap){
//        int s = 1/0;

        //封装页面显示数据
        List<City> cities = cityService.getCitiesByCountryId(522);
        cities = cities.stream().limit(10).collect(Collectors.toList());
        modelMap.addAttribute("title", "Thymeleaf Page");
        modelMap.addAttribute("checked", true);
        modelMap.addAttribute("currentPage", 100);
        modelMap.addAttribute("baidu", "http://www.baidu.com");
        modelMap.addAttribute("city", cities.get(0));
        modelMap.addAttribute("cities", cities);
        modelMap.addAttribute("template","test/thymeleafTest");
        return "index";
    }

    /**
     * @param req
     * @param name
     * 127.0.0.1/test/helloWorld?name=fucktheworld ---get
     */
    @GetMapping(value = "/helloWorld")
    @ResponseBody
    public String helloWorld(HttpServletRequest req, @RequestParam String name){

//        int s = 1/0;
        //HttpServletRequest---获得的也是name的值，可以使用getParameter进行处理
        //@RequestParam----获得的也是name的值，不能使用getParameter方法处理，只能使用getParameterValues处理
        //即getParameter可处理HttpServletRequest，getParameterValues处理所有
        String name1 = req.getParameter("name");
        return "index"+name+","+name1;
    }
}
