package com.wyb.spring.modules.account.service;

import com.github.pagehelper.PageInfo;

import com.wyb.spring.modules.account.entity.Role;
import com.wyb.spring.modules.common.vo.Result;
import com.wyb.spring.modules.common.vo.Search;

import java.util.List;

/**
 * @Description: Role Service
 * @author: HymanHu
 * @date: 2021年2月21日
 */
public interface RoleService {
	
	Result<Role> insertRole(Role role);

	Result<Role> updateRole(Role role);

	Result<Object> deleteRoelById(int id);
	
	Role getRoleById(int id);
	
	List<Role> getRolesByUserId(int userId);
	
	List<Role> getRoles();
	
	PageInfo<Role> getRolesBySearchBean(Search search);
}
