package com.wyb.spring.modules.account.entity;

import com.wyb.spring.modules.common.entity.AbstactEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @Description Role
 * @Author JiangHu
 * @Date 2022/9/20 14:09
 */
@Entity
@Table(name = "account_role")
public class Role extends AbstactEntity {
	private String roleName;

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
}
