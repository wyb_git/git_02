package com.wyb.spring.modules.common.controller;

import com.wyb.spring.modules.common.vo.ImageEnum;
import com.wyb.spring.modules.common.vo.Result;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author lenovo
 * @Date 2022/9/20 11:18
 * @Version 1.0
 * @Function
 */
@RestController
@RequestMapping("/api")
public class DictionaryController {
    private static Map<String, Class> enumMap = new HashMap(){{
        put("imageEnum", ImageEnum.class);
        put("resultStatus", Result.ResultStatus.class);
    }};


    @GetMapping("/dictionary/{key}")
    public List<Object> getEnum(@PathVariable String key) {
        try {
            if (enumMap.containsKey(key)) {
                Class clazz = enumMap.get(key);
                Method method = clazz.getDeclaredMethod("values");
                //因为没有使用构造器，则invoke第一个参数没有，第二个参数为你对应方法的参数
                Object[] objects = (Object[]) method.invoke(null);
                return Arrays.asList(objects);
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }
}
