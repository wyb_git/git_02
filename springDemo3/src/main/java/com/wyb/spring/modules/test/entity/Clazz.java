package com.wyb.spring.modules.test.entity;

import com.wyb.spring.modules.common.entity.AbstactEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

/**
 * @Author lenovo
 * @Date 2022/9/13 16:47
 * @Version 1.0
 * @Function
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "test_clazz")
public class Clazz extends AbstactEntity {
    private String clazzName;
    @ManyToOne(targetEntity = School.class,cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @JoinColumn(name = "school_id",unique = true)
    private School school;

    @ManyToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @JoinTable(name = "test_clazz_student",joinColumns = @JoinColumn(name = "clazz_id"),
    inverseJoinColumns = @JoinColumn(name = "student_id"))
    private List<Student> students;
}
