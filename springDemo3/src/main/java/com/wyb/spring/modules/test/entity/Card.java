package com.wyb.spring.modules.test.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wyb.spring.modules.common.entity.AbstactEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * @Author lenovo
 * @Date 2022/9/13 16:41
 * @Version 1.0
 * @Function
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "test_card")
public class Card extends AbstactEntity {
    private String cardNo;
    // 没有外键的一方，使用 mappedBy 标注另一方关联属性名
    @OneToOne(mappedBy = "studentCard")
    @JsonIgnore
    private Student student;
}
