package com.wyb.spring.modules.account.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import com.sfac.springBoot.utils.JwtUtil;
import com.wyb.spring.modules.account.dao.UserDao;
import com.wyb.spring.modules.account.dao.UserRoleDao;
import com.wyb.spring.modules.account.entity.User;
import com.wyb.spring.modules.account.entity.UserRole;
import com.wyb.spring.modules.account.service.UserService;
import com.wyb.spring.modules.common.vo.Result;
import com.wyb.spring.modules.common.vo.Search;
import com.wyb.spring.utils.MD5Util;
import com.wyb.spring.utils.RedisUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @Description: User Service Impl
 * @author: HymanHu
 * @date: 2021年2月21日
 */
@Service
public class UserServiceImpl implements UserService {
	public final static Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

	@Autowired
	private UserDao userDao;
	@Autowired
	private UserRoleDao userRoleDao;
	@Autowired
	private RedisUtils redisUtils;

	@Override
	public User getUserByUserNameAndPassword(String userName, String password) {
		return userDao.getUserByUserNameAndPassword(userName, MD5Util.getMD5(password));
	}

	@Override
	public User getUserByUserName(String userName) {
		List<User> users = Optional
				.ofNullable(userDao.getUserByUserName(userName))
				.orElse(Collections.emptyList());
		return users.isEmpty() ? null : users.get(0);
	}

	@Override
	public Result<User> login(User user) {
		/*User temp = userDao.getUserByUserNameAndPassword(
				user.getUserName(), MD5Util.getMD5(user.getPassword()));
		if (temp != null) {
			return new Result<User>(Result.ResultStatus.SUCCESS.code, "登录成功");
		} else {
			return new Result<User>(Result.ResultStatus.FAILED.code, "用户名或密码错误");
		}*/

		String key = String.format("error_login_count_%s", user.getUserName());
		int maxErrorCount = 3;
		int lockAccountSecond = 30;

		// 获得错误登录次数，大于最大值，直接返回结果集
		int count = redisUtils.get(key) == null ? 0 : (Integer) redisUtils.get(key);
		if (count >= maxErrorCount) {
			return new Result<>(Result.ResultStatus.FAILED.code,
					String.format("连续错误登录 %d 次，锁定账户 %d 秒", maxErrorCount, lockAccountSecond));
		}

		// 获取 subject 组件
		Subject subject = SecurityUtils.getSubject();

		// 包装令牌
		UsernamePasswordToken token = new UsernamePasswordToken(
				user.getUserName(), MD5Util.getMD5(user.getPassword()));

		try {
			// 调用登录方法
			subject.login(token);

			// 资源授权
			subject.checkRoles();

			// 包装 session 数据
			Session session = subject.getSession();
			session.setAttribute("user", subject.getPrincipal());

			// 清除错误登录次数
			redisUtils.set(key, 0);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.debug(e.getMessage());

			// 增加错误登录次数，根据 count 进行业务处理
			count++;
			redisUtils.increment(key, 1);
			if (count >= maxErrorCount) {
				redisUtils.expire(key, lockAccountSecond);
				return new Result<>(Result.ResultStatus.FAILED.code,
						String.format("连续错误登录 %d 次，锁定账户 %d 秒", maxErrorCount, lockAccountSecond));
			} else {
				return new Result<>(Result.ResultStatus.FAILED.code,
						String.format("已连续错误登录 %d 次，还剩 %d 次机会", count, (maxErrorCount - count)));
			}
		}

		return new Result<User>(Result.ResultStatus.SUCCESS.code, "登录成功");
	}

	@Override
	public Result<String> login2(User user) {
		User temp = userDao.getUserByUserNameAndPassword(
				user.getUserName(), MD5Util.getMD5(user.getPassword()));
		if (temp == null) {
			return new Result<>(Result.ResultStatus.FAILED.code, "用户名或密码错误");
		}
		String jwtToken = JwtUtil.createUserToken(temp.getId() + "", temp.getUserName());
		return new Result<>(Result.ResultStatus.SUCCESS.code, "登录成功",jwtToken);
	}

	@Override
	public void logout() {
		Subject subject = SecurityUtils.getSubject();
		subject.logout();
	}

	@Override
	@Transactional
	public Result<User> insertUser(User user) {
		if (StringUtils.isBlank(user.getUserName()) ||
				StringUtils.isBlank(user.getPassword())) {
			return new Result<User>(Result.ResultStatus.FAILED.code,
					"User Name is null.");
		}
		
		List<User> users = Optional
				.ofNullable(userDao.getUserByUserName(user.getUserName()))
				.orElse(Collections.emptyList());
		if (users.size() > 0) {
			return new Result<User>(Result.ResultStatus.FAILED.code,
					"User name repeat.");
		}
		
		user.setCreateDateTime(LocalDateTime.now());
		user.setUpdateDateTime(LocalDateTime.now());
		user.setPassword(MD5Util.getMD5(user.getPassword()));
		userDao.insertUser(user);
		if (user.getRoles() != null) {
			user.getRoles().stream()
				.forEach(item -> {userRoleDao.insertUserRole(
						new UserRole(user.getId(), item.getId()));});
		}
		return new Result<User>(
				Result.ResultStatus.SUCCESS.code, "Insert success", user);
	}

	@Override
	@Transactional
	public Result<User> updateUser(User user) {
		List<User> users = Optional
				.ofNullable(userDao.getUserByUserName(user.getUserName()))
				.orElse(Collections.emptyList());
		if (users.stream().filter(item -> item.getId() != user.getId()).count() > 0) {
			return new Result<User>(Result.ResultStatus.FAILED.code,
					"User Name or email is repeat.");
		}
		
		userDao.updateUser(user);
		if (user.getRoles() != null && !user.getRoles().isEmpty()) {
			userRoleDao.deleteUserRoleByUserId(user.getId());
			user.getRoles().stream()
				.forEach(item -> {userRoleDao.insertUserRole(
						new UserRole(user.getId(), item.getId()));});
		}
		
		return new Result<User>(Result.ResultStatus.SUCCESS.code,
				"Update success", user);
	}

	@Override
	public User getUserById(int id) {
		return userDao.getUserById(id);
	}

	@Override
	@Transactional
	public Result<Object> deleteUserById(int id) {
		userDao.deleteUserById(id);
		userRoleDao.deleteUserRoleByUserId(id);
		return new Result<Object>(Result.ResultStatus.SUCCESS.code, "Delete success");
	}

	@Override
	public PageInfo<User> getUsersBySearchBean(Search search) {
		search.init();
		PageHelper.startPage(search.getCurrentPage(), search.getPageSize());
		return new PageInfo<User>(Optional
				.ofNullable(userDao.getUsersBySearchVo(search))
				.orElse(Collections.emptyList()));
	}

	@Override
	public List<User> getUsersByKeyWord(String keyword) {
		Search search = new Search();
		search.setKeyword(keyword);
		search.init();
		return Optional.ofNullable(userDao.getUsersBySearchVo(search))
				.orElse(Collections.emptyList());
	}

}
