package com.wyb.spring.modules.account.entity;

import com.wyb.spring.modules.common.entity.AbstactEntity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.List;

/**
 * @Description User
 * @Author JiangHu
 * @Date 2022/9/20 14:05
 */
@Entity
@Table(name = "account_user")
public class User extends AbstactEntity {
	private String userName;
	private String password;
	private String userImage;
	@Transient
	private List<Role> roles;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserImage() {
		return userImage;
	}

	public void setUserImage(String userImage) {
		this.userImage = userImage;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
}
