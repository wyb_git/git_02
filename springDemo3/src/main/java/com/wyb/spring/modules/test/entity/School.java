package com.wyb.spring.modules.test.entity;

import com.wyb.spring.modules.common.entity.AbstactEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

/**
 * @Author lenovo
 * @Date 2022/9/13 16:48
 * @Version 1.0
 * @Function
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "test_school")
public class School extends AbstactEntity {
    private String schoolName;

    @OneToMany(mappedBy = "school",cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    private List<Clazz> clazzList;
}