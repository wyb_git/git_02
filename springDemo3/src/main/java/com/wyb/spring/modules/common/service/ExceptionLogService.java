package com.wyb.spring.modules.common.service;

import com.wyb.spring.modules.common.entity.ExceptionLog;
import com.wyb.spring.modules.common.vo.Result;

/**
 * @Author lenovo
 * @Date 2022/9/20 16:08
 * @Version 1.0
 * @Function
 */
public interface ExceptionLogService {
    Result<ExceptionLog> insertExceptionLog(ExceptionLog ex);
}
