package com.wyb.spring.modules.test.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wyb.spring.modules.common.entity.AbstactEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

/**
 * @Author lenovo
 * @Date 2022/9/13 16:45
 * @Version 1.0
 * @Function
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "test_student")
public class Student extends AbstactEntity {
    private String name;
    private int age;
    // 一对一关系中，使用外键的一方用 targetEntity，标注目标类模板,后面依次为级联策略和废弃策略
    @OneToOne(targetEntity = Card.class,cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    // 使用 JoinColumn 标注外键列名
    @JoinColumn(name = "card_id",unique = true)
    private Card studentCard;

    @ManyToMany(mappedBy = "students",cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @JsonIgnore
    private List<Clazz> clazzes;
}
