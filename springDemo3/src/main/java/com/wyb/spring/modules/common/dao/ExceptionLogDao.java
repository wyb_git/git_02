package com.wyb.spring.modules.common.dao;

import com.wyb.spring.modules.common.entity.ExceptionLog;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

/**
 * @Author lenovo
 * @Date 2022/9/20 11:49
 * @Version 1.0
 * @Function
 */
@Mapper
@Repository
public interface ExceptionLogDao {
    @Insert("insert into common_exception_log (create_date_time,update_date_time,ip,path,class_name," +
            "method_name,exception_type,exception_message)" +
            " values(#{createDateTime},#{updateDateTime},#{ip},#{path},#{className},#{methodName},#{exceptionType},#{exceptionMessage})")
    @Options(useGeneratedKeys = true,keyProperty = "id",keyColumn = "id")
    void insertExceptionLog(ExceptionLog ex);

    @Select("select * from common_exception_log where path = #{path} and method_name = #{methodName}" +
            " and exception_type = #{exceptionType} limit 1")
    ExceptionLog getExceptionLogByParam(@Param("path") String path,@Param("methodName") String methodName,
                                        @Param("exceptionType") String exceptionType);
}