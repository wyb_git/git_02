package com.wyb.spring.modules.test.dao;

import com.wyb.spring.modules.test.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @Author lenovo
 * @Date 2022/9/13 17:49
 * @Version 1.0
 * @Function
 */
@Repository
public interface StudentDao extends JpaRepository<Student,Integer> {

    // findBy + 属性 + 关键字 + 属性（option）；
    // find + Top || First（option） + By + 属性 + 关键字 + 属性（option）；
    Student findFirstByName(String name);

    //使用原生SQL语句，设置true，但默认为false，即默认使用HQL
    //@Query(nativeQuery = true,value = "select * from test_student where id = :id")
    @Query(value = "from Student where id = :id")
    Student getStudentById(@Param("id") int id);


    @Modifying
    @Query(nativeQuery = true,value = "update test_student set name=:#{#student.name}," +
            "age = :#{#student.age} where id = :#{#student.id}")
    void updateStudent(@Param("student") Student student);
}
