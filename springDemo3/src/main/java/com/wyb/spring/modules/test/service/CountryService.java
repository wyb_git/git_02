package com.wyb.spring.modules.test.service;

import com.github.pagehelper.PageInfo;
import com.wyb.spring.modules.common.vo.Search;
import com.wyb.spring.modules.test.entity.City;
import com.wyb.spring.modules.test.entity.Country;

/**
 * @Author lenovo
 * @Date 2022/9/10 8:55
 * @Version 1.0
 * @Function
 */
public interface CountryService {

    Country getCountryByCountryId(int countryId);


}
