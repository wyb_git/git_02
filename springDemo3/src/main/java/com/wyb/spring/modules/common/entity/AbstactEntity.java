package com.wyb.spring.modules.common.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @Author lenovo
 * @Date 2022/9/13 16:31
 * @Version 1.0
 * @Function
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@MappedSuperclass
//该注解为了是更新数据时只更新局部数据，其他没设置的数据不会置空
@EntityListeners(AuditingEntityListener.class)
public class AbstactEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false,nullable = false)
    private Integer id;
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss") //该注解是为了能支持前端传递表单数据
    //该注解为了是更新数据时只更新局部数据，其他没设置的数据不会置空
    @CreatedDate
    private LocalDateTime createDateTime;
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss") //该注解是为了能支持前端传递表单数据
    //该注解为了是更新数据时只更新局部数据，其他没设置的数据不会置空
    @LastModifiedDate
    private LocalDateTime updateDateTime;

    @PrePersist
    public void preInsert(){
        setCreateDateTime(LocalDateTime.now());
        setUpdateDateTime(LocalDateTime.now());
    }

}
