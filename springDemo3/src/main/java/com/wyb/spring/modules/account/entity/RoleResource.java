package com.wyb.spring.modules.account.entity;

import javax.persistence.*;

/**
 * @Description RoleResource
 * @Author JiangHu
 * @Date 2022/9/20 14:12
 */
@Entity
@Table(name = "account_role_resource")
public class RoleResource {
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(updatable = false, nullable = true)
	private Integer id;
	private int roleId;
	private int resourceId;

	public RoleResource() {
	}

	public RoleResource(int roleId, int resourceId) {
		this.roleId = roleId;
		this.resourceId = resourceId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public int getResourceId() {
		return resourceId;
	}

	public void setResourceId(int resourceId) {
		this.resourceId = resourceId;
	}
}
