package com.wyb.spring.modules.account.entity;


import com.wyb.spring.modules.common.entity.AbstactEntity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.List;

/**
 * @Description Resource
 * @Author JiangHu
 * @Date 2022/9/20 14:10
 */
@Entity
@Table(name = "account_resource")
public class Resource extends AbstactEntity {
	private String permission;
	@Transient
	private List<Role> roles;

	public String getPermission() {
		return permission;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
}
