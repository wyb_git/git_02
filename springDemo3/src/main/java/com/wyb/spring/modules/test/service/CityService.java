package com.wyb.spring.modules.test.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.pagehelper.PageInfo;
import com.wyb.spring.modules.common.vo.Result;
import com.wyb.spring.modules.common.vo.Search;
import com.wyb.spring.modules.test.entity.City;

import java.util.List;

/**
 * City Service
 */
public interface CityService {

	Result<City> insertCity(City city);

	Result<City> updateCity(City city);

	Result<City> deleteCityByCityId(int cityId);

	City getCityByCityId(int cityId);

	List<City> getCitiesByCountryId(int countryId);

	PageInfo<City> getCityBySearch(Search search);

	Page<City> getCityBySearch2(Search search);
}
