package com.wyb.spring.modules.common.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @Author lenovo
 * @Date 2022/9/20 11:46
 * @Version 1.0
 * @Function
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "common_exception_log")
public class ExceptionLog extends AbstactEntity{
    private String ip;
    private String path;
    private String className;
    private String methodName;
    private String exceptionType;
    private String exceptionMessage;
}
