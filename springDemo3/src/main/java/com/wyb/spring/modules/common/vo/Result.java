package com.wyb.spring.modules.common.vo;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * @Description Result
 * @Author JiangHu
 * @Date 2022/9/9 10:26
 */
public class Result<T> {
	private int status;
	private String message;
	private T data;

	public Result() {
	}

	public Result(int status, String message) {
		this.status = status;
		this.message = message;
	}

	public Result(int status, String message, T data) {
		this.status = status;
		this.message = message;
		this.data = data;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	@JsonFormat(shape = JsonFormat.Shape.OBJECT)
	public enum ResultStatus {
		// TODO 定义枚举
		SUCCESS(200),
		FAILED(500);

		// 定义属性
		public int code;

		// 定义构造器
		ResultStatus(int code) {
			this.code = code;
		}
	}
}
