package com.wyb.spring.modules.common.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Author lenovo
 * @Date 2022/9/20 11:45
 * @Version 1.0
 * @Function
 */
@Controller
@RequestMapping("/common")
public class CommonController {

    /**
     * 127.0.0.1/common/403 ---- get
     */
    @GetMapping("/403")
    public String errorPageFor403() {
        return "index2";
    }

    /**
     * 127.0.0.1/common/404 ---- get
     */
    @GetMapping("/404")
    public String errorPageFor404() {
        return "index2";
    }

    /**
     * 127.0.0.1/common/500 ---- get
     */
    @GetMapping("/500")
    public String errorPageFor500() {
        return "index2";
    }
}
