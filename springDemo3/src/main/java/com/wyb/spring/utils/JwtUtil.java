package com.sfac.springBoot.utils;

import java.util.Date;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;

/**
 * Description: Jwt Util
 * @author HymanHu
 * @date 2022-02-23 16:18:19
 */
public class JwtUtil {
	private static final Long EXPIRES_TIME = 1 * 60 * 60 * 1000L;
	public static final String TOKEN_SALT = "HuJiang";
	
	/**
	 * -创建用户令牌
	 * @param id			JWT 唯一身份标识，使用 userId、UUID 等
	 * @param userName		用户名，自定义载荷内容
	 * @param expTime		过期时间，毫秒
	 * @return token
	 */
	public static String createUserToken(String id, String userName, long expTime) {
		long now = System.currentTimeMillis();
		Date currentDate = new Date(now);
		Date expDate = new Date(now + expTime);
		
		return JWT.create()
			.withAudience(id) // 签发对象
			.withIssuedAt(currentDate) // 发行时间
			.withExpiresAt(expDate) // 有效期
			.withClaim("userName", userName) // Payload 载荷，可按官方推荐，也可自定义，可有多个
			.sign(Algorithm.HMAC256(id + TOKEN_SALT)); // 使用 HMAC256 加密算法，生成签名
	}

	public static String createUserToken(String id, String userName) {
		return createUserToken(id, userName, EXPIRES_TIME);
	}
	
	/**
	 * - 验证令牌
	 */
	public static DecodedJWT verifyToken(String token, String secret) {
		DecodedJWT decodedJWT = null;
		try {
			JWTVerifier jWTVerifier = JWT.require(Algorithm.HMAC256(secret + TOKEN_SALT)).build();
			decodedJWT = jWTVerifier.verify(token);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return decodedJWT;
	}
	
	/**
	 * - 获取令牌中的签发对象
	 */
	public static String getAudience(String token) {
		String audience = null;
		try {
			audience = JWT.decode(token).getAudience().get(0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return audience;
	}
	
	/**
	 * - 根据名字获取载荷内容
	 */
	public static Claim getClaimByName(String token, String claimName) {
		return JWT.decode(token).getClaim(claimName);
	}
	
	public static void main(String[] args) {
		String token = JwtUtil.createUserToken("1", "admin", 30 * 60 * 1000);
		System.out.println(token);
		System.out.println(JwtUtil.verifyToken(token, "1").getClaims());
		System.out.println(JwtUtil.verifyToken(token, "2"));
		System.out.println(JwtUtil.getAudience(token));
		System.out.println(JwtUtil.getClaimByName(token, "userName"));
	}
}
