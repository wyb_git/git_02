package com.wyb.spring.listener;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @Author lenovo
 * @Date 2022/9/15 16:10
 * @Version 1.0
 * @Function
 */
@WebListener
public class LogFileDestroyListener implements ServletContextListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(LogFileDestroyListener.class);


    private String logBaseFolder="/var/log";

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        File destFolder = new File(logBaseFolder);
        if (!destFolder.exists()) {
            return;
        }
        //拿到当前日期格式化字符串
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String currentDate = dtf.format(LocalDateTime.now());
        //将目标文件夹的日志文件进行遍历，不满足条件的删除
        File[] files = destFolder.listFiles();
        for (File file: files
             ) {
            String fileName = file.getName();
            if (!fileName.contains(currentDate)) {
                file.delete();
            }
        }
        LOGGER.debug("debug is log");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        LOGGER.error("error is log");
    }
}
