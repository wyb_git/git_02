package com.wyb.spring.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

/**
 * @Author lenovo
 * @Date 2022/9/15 16:42
 * @Version 1.0
 * @Function
 */
@WebFilter(filterName = "parameterFilter",urlPatterns = "/*")
public class ParameterFilter implements Filter {

    private static final Logger LOGGER = LoggerFactory.getLogger(ParameterFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        Filter.super.init(filterConfig);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;

        // request 对象获取所有的查询参数
        // req.getParameterMap() ---- 返回的 map 对象是只读的 map，不能改变其中的值
        // request 对象，不允许增删改参数的，使用 HttpServletRequestWrapper 来操作
        // 重写 wapper 实现接口中 getParameter 方法，然后将 wapper 对象交出去
        // 重写方式：1、创建一个类继承 wapper，重写方法；2、直接在创建对象时重写
        HttpServletRequestWrapper wrapper = new HttpServletRequestWrapper(req){
            @Override
            public String getParameter(String name) {
                String value = super.getParameter(name);
                if (value.contains("fuck")) {
                    return value.replaceAll("fuck", "****");
                }
                return value;
            }

            @Override
            public String[] getParameterValues(String name) {
                String[] names = super.getParameterValues(name);
               /* for (String name1:names
                     ) {
                    if (name1.contains("fuck")) {
                        name1.replaceAll("fuck", "****");
                    }
                }
                return names;*/

                //先将数组转map，在全部替换，在转为数组放在新的数组中（使用方法的引用的构造方法方式创建的空数组）
                return Arrays.asList(names)
                        .stream()
                        .map(item -> item.replaceAll("fuck", "****"))
                        .toArray(String[] :: new);
            }
        };
        filterChain.doFilter(wrapper,resp);
    }

    @Override
    public void destroy() {
        Filter.super.destroy();
    }
}
