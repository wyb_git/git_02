package com.wyb.spring.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author lenovo
 * @Date 2022/9/16 12:33
 * @Version 1.0
 * @Function
 */
@Aspect
@Component
public class MyAOP {
    private static final Logger LOGGER = LoggerFactory.getLogger(MyAOP.class);

    @Pointcut("execution(public * com.wyb.spring.modules.*.controller.*.*(..))")
    @Order(1)
    public void controllerPointcut(){}

    @Around(value = "controllerPointcut()")
    public Object aroundController(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        LOGGER.debug("-----around------");
        return proceedingJoinPoint.proceed(proceedingJoinPoint.getArgs());
    }

    //注解切点
    @Pointcut("@annotation(com.wyb.spring.aop.MyAnnotation)")
    @Order(1)
    public void annotationPointcut(){}


    @Before(value = "controllerPointcut()")
    public void beforeController(JoinPoint joinPoint) {
        LOGGER.debug("-----before------");
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest req = servletRequestAttributes.getRequest();
        LOGGER.debug(String.format("请求 URL： %s",req.getRequestURL()));
        LOGGER.debug(String.format("请求 Method： %s",req.getMethod()));
        LOGGER.debug(String.format("请求类： %s",joinPoint.getTarget().getClass().getName()));
        LOGGER.debug(String.format("请求方法： %s",joinPoint.getSignature().getName()));
    }


    @After(value = "controllerPointcut()")
    public void afterController(JoinPoint joinPoint) {
        LOGGER.debug("-----after------");
    }


    @Around(value = "controllerPointcut()")
    public Object aroundAnnotation(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        LOGGER.debug("-----around annotation------");
        long start = System.currentTimeMillis();
        Object proceed = proceedingJoinPoint.proceed(proceedingJoinPoint.getArgs());
        long end = System.currentTimeMillis();
        LOGGER.debug("方法执行时间：" + (end - start));
        return proceed;
    }
}
